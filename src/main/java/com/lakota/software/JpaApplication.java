package com.lakota.software;

import com.lakota.software.book.Book;
import com.lakota.software.book.BookRepository;
import com.lakota.software.book.PublisherRepository;
import com.lakota.software.book.Publisher;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@Slf4j
@SpringBootApplication
public class JpaApplication implements CommandLineRunner{
	@Autowired
	private BookRepository bookRepository;
	private PublisherRepository publisherRepository;
	
	public static void main(String[] args) {
		SpringApplication.run(JpaApplication.class, args);
	}
	
	@Override
	public void run(String... args) {
		//Create a couple of Books and Publishers
		bookRepository.save(new Book("Book 1", new Publisher("Publisher A"), new Publisher("Publisher B")));
		publisherRepository.save(new Publisher(""));
	}
}
